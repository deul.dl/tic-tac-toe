import React from 'react';
import axios from 'axios';

import './App.css';


function App() {
  const [main, setMain] = React.useState();
  React.useEffect(() => {
    const intervalId = setInterval(async () => {
      setMain((await axios.get('http://localhost:3300/main')).data.data);
    }, 1000);
    return () => clearInterval(intervalId);
  }, []);
  
  const handleSelectPosition = async (y, x) => {
    axios.post('http://localhost:3300/doStep', { y, x })
      .then((res) => {
        if (res.data.ok) {
          if (res.data.data) {
            alert('Win player who role is ' + res.data.data)
          }
        }
      })
      .catch((err) => alert(err.response.data.message));
  };

  const handleReset = async () => {
    axios.get('http://localhost:3300/reset');
  };

  if (!main) return <></>;

  return (
    <main className="background">
      <section className="title">
        <h1>Tic Tac Toe</h1>
      </section>
      <section className="display">
        Player <span className="display-player">{main.current.role}</span>'s turn
      </section>
      <section className="display">
        Player 1 <span className="display-player playerO">({main.player1.role})</span>  <span className="display-player playerO">{main.player1.count}</span>
        <br />
        Player 2 <span className="display-player playerX">({main.player2.role})</span> <span className="display-player playerX">{main.player2.count}</span>
      </section>
      <section className="container">
        <div onClick={() => handleSelectPosition(0, 0)} className="tile">{main.position[0][0]}</div>
        <div onClick={() => handleSelectPosition(0, 1)} className="tile">{main.position[0][1]}</div>
        <div onClick={() => handleSelectPosition(0, 2)} className="tile">{main.position[0][2]}</div>
        <div onClick={() => handleSelectPosition(1, 0)} className="tile">{main.position[1][0]}</div>
        <div onClick={() => handleSelectPosition(1, 1)} className="tile">{main.position[1][1]}</div>
        <div onClick={() => handleSelectPosition(1, 2)} className="tile">{main.position[1][2]}</div>
        <div onClick={() => handleSelectPosition(2, 0)} className="tile">{main.position[2][0]}</div>
        <div onClick={() => handleSelectPosition(2, 1)} className="tile">{main.position[2][1]}</div>
        <div onClick={() => handleSelectPosition(2, 2)} className="tile">{main.position[2][2]}</div>
      </section>
      <section className="display announcer hide"></section>
      <section className="controls">
        <button onClick={handleReset} id="reset">Reset</button>
      </section>
    </main>
  );
}

export default App;
