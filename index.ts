import http from 'http';
import bodyParser from 'body-parser';
import express from 'express';
import cors from 'cors';

import PlayingSession from './game';

const app: express.Application = express();

const cache: Array<PlayingSession> = [];

app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(function (req, res, next) {
	res.header('Access-Control-Allow-Credentials', 'http://127.0.0.1');
	res.header('Access-Control-Allow-Origin', 'http://127.0.0.1');
	res.header('Access-Control-Allow-Headers', Object.keys(Object.assign({ 'content-type': 1 }, req.headers)).join(','));
	res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS');
	next();
});

app.use(cors({ origin: true, credentials: true }));

interface PositionXY {
	x: number,
	y: number
}

app
	.get('/main', (req: express.Request, res: express.Response) => {
		let session: PlayingSession;
		if (cache[0]) {
			session = cache[0];
		} else {
      session = new PlayingSession();
      cache.push(session);
		}
    return res.status(200).json({ ok: true, data: session.toJSON() });
  })
	.post('/doStep', (req: express.Request, res: express.Response) => {
		const { x, y }: PositionXY = req.body;
		if (typeof x !== 'number') return res.status(400).json({ message: 'x invalid', ok: false });
		if (typeof y !== 'number') return res.status(400).json({ message: 'y invalid', ok: false });
		let session: PlayingSession;
		if (!cache[0]) {
			session = new PlayingSession();
      cache.push(session);
		}
		 session = cache[0];
		try {
			const role: string | void = session.doStep(y, x);
			return res.status(200).json({ ok: true, data: role });
		} catch (err) {
			return res.status(400).json({ ok: false, message: (err as Error).message });
		}
	})
	.get('/reset', (req: express.Request, res: express.Response) => {
		let session: PlayingSession;
		if (!cache[0]) {
			session = new PlayingSession();
      cache.push(session);
		}
		session = cache[0];
		session.reset();
		return res.status(200).json({ ok: true });
	})
	.get('/*', async (_, res) => {
		return res.status(404).json({ message: 'not found', data: null });
	});

const server = http.createServer(app);

const callbackListen = (): void => {
	const addr = server.address();
	const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr?.port}`;
	console.log(`Listening on ${bind}`);
}

server.listen(3300, callbackListen);
