export class Player {
  public role: string;
  public count: number;

  constructor (role: string) {
    if (!role) throw new Error('Role is empty');

    this.role = role; // x or 0

    this.count = 0;
  }

  public addCount (): void {
    this.count++;
  }

  public reset(): void {
    this.count = 0;
  }
}

class Position3x3 {
  __xy: Array<Array<string>>;

  constructor () {
    this.__xy = [['', '', ''], ['', '', ''], ['', '', '']];
  }

  public validRules (role: string): boolean {
    if (this.__xy[0][0] === role && this.__xy[0][1] === role && this.__xy[0][2] === role) {
      return true;
    } else if (this.__xy[1][0] === role &&  this.__xy[1][1] === role && this.__xy[1][2] === role) {
      return true;
    } else if (this.__xy[2][0] === role &&  this.__xy[2][1] === role && this.__xy[2][2] === role) {
      return true;
    } else if (this.__xy[0][0] === role &&  this.__xy[1][0] === role && this.__xy[2][0] === role) {
      return true;
    } else if (this.__xy[0][1] === role &&  this.__xy[1][1] === role && this.__xy[2][1] === role) {
      return true;
    } else if (this.__xy[0][2] === role &&  this.__xy[1][2] === role && this.__xy[2][2] === role) {
      return true;
    } else if (this.__xy[0][0] === role &&  this.__xy[1][1] === role && this.__xy[2][2] === role) {
      return true;
    } else if (this.__xy[0][2] === role &&  this.__xy[1][1] === role && this.__xy[2][0] === role) {
      return true;
    }
    return false;
  }

  public setPosition(y: number, x: number, role: string): never | void {
    if (this.__xy[y][x] !== '') throw new Error('Position is busy');
    this.__xy[y][x] = role;
  }
}

interface IPlayer {
  count: number,
  role: string
}

interface SessionJSON {
  player1: IPlayer,
  player2: IPlayer,
  current: IPlayer,
  position: Array<Array<string>>
}

export default class PlayingSession {
    public player1: Player;
    public player2: Player;
    public position: Position3x3;
    public current: Player;

    private quantitySteps: number;
    private comboWin: Array<String>;
    private invervalId: NodeJS.Timer;
    // я не уверен что тут надо, сделал бы за класс вообще,
    // но тут я подумал что нужно все поконтролировать, когда игра закончится, когда выиграет и лучше все го тут сделать. 
    // Может я прав а может и да.

  constructor() {
    this.player1 = new Player('0');
    this.player2 = new Player('x');
  
    this.current = this.player1;
  
    this.quantitySteps = 0;
    this.position = this.generatePosition();
    this.comboWin = [];

    this.invervalId = setTimeout(() => {
      this.skipStep();
    }, 15000);
  }

  public doStep (y: number, x: number): void | string {
    clearTimeout(this.invervalId);
    this.position.setPosition(y, x, this.current.role);
    this.quantitySteps +=1;
    if (this.position.validRules(this.current.role)) {
      return this.win();
    } else if (this.quantitySteps === 9) {
      this.restart();
    } else {
      if (this.current.role === '0') {
        this.current = this.player2;
      } else {
        this.current = this.player1;
      }
      this.invervalId = setTimeout(() => {
        this.skipStep();
      }, 15000);
    }
  }

  public reset (): void {
    clearTimeout(this.invervalId);
    this.player1.reset();
    this.player2.reset();
    this.position = this.generatePosition();
    this.comboWin = [];
    this.quantitySteps = 0;
    this.invervalId = setTimeout(() => {
      this.skipStep();
    }, 15000);
  }

  public skipStep (): void {
    if (this.current.role === '0') {
      this.current = this.player2;
    } else {
      this.current = this.player1;
    }
  }

  private restart (): void {
    this.invervalId = setTimeout(() => {
      this.skipStep();
    }, 15000);

    this.current = this.player1;

    this.quantitySteps = 0;
    this.position = this.generatePosition();
  }
  // Можно еще убрать объект Player, сделать массив winners, оттуда считать 3 победы подряд или 10 побед по методу перебору, тогда мыы сократим данные,
  // тогда за место current будет просто x or y,  я думаю тогда даже так лучше поскольку не вижу смысла в игроке один или в игроке 2, но не буду переделавать
  // как это тестовое задания.
  private win (): string {
    this.current.addCount();
    if (this.current.count === 10) {
      this.reset();
      return this.current.role;
    } else if (this.comboWin[this.comboWin.length - 1] == this.current.role
        && this.comboWin[this.comboWin.length - 2] == this.current.role) {
      this.reset();
      return this.current.role;
    }
    this.comboWin.push(this.current.role);
    this.restart();
    return '';
  }

  private generatePosition (): Position3x3 {
    return new Position3x3();
  }

  public toJSON (): SessionJSON {
    return {
      player1: {
        role: this.player1.role,
        count: this.player1.count
      },
      player2: {
        role: this.player1.role,
        count: this.player2.count
      },
      current: {
        role: this.current.role,
        count: this.current.count
      },
      position: this.position.__xy,
    }
  }
};
